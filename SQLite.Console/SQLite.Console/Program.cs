﻿using Microsoft.Data.Sqlite;
using System;
using System.Diagnostics;

namespace SQLite.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            DisplayProductsUsingAdo();
        }

        private static void DisplayProductsUsingAdo()
        {
            SqliteConnection conn = new SqliteConnection(@"Data Source=C:\GrowthAndLearning\SQLite.Console\sqlite.console\SQLite.Console\SQLite.Console\DB\ChampionProductsDB.db");
            conn.Open();

            SqliteDataReader datareader;
            SqliteCommand command;

            using (conn)
            {
                command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM product";

                datareader = command.ExecuteReader();
                while (datareader.Read())
                {
                    string product = datareader.GetString(1);
                    System.Console.WriteLine(product);
                }
                
            }

            System.Console.ReadLine();
        }
    }
}
